=== Plugin Name ===

Contributors: (ralbatross)
Donate link: https://www.innovative.ink
Tags: varius
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Pull your Varius listings into your WordPress website.

== Description ==

If you have a Varius account where you host used equipment listings you can use this plugin to pull those listings into your WordPress website.

== Installation ==



== Screenshots ==


== Changelog ==

