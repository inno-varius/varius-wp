<?php

require __DIR__ . '/vendor/autoload.php'; // Use composer's autoload

use VariusApi\VariusApi;

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.innovative.ink
 * @since             1.0.0
 * @package           Varius_Wp
 *
 * @wordpress-plugin
 * Plugin Name:       Varius-WP
 * Plugin URI:        https://varius.ca
 * Description:       Integrate your Varius listings into your WordPress website.
 * Version:           1.0.0
 * Author:            Innovative
 * Author URI:        https://www.innovative.ink
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       varius
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Current plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );


/**
 * Name of WordPress option for Varius WP plugin settings
 */
define('VARIUS_WP_SETTINGS', 'varius-wp-settings');

/**
 * See Varius_Wp::define_admin_hooks
 */
define('VARIUS_WP_DO_FLUSH_REWRITE_RULES', 'varius-wp-do-flush-rewrite-rules');

define('VARIUS_WP_OPTION_LAST_SYNC', 'varius-wp-last-sync');
define('VARIUS_WP_OPTION_VARIUS_TYPES', 'varius-wp-varius-types');
define('VARIUS_WP_OPTION_LISTING_CATEGORY_CUSTOM_FIELDS', 'varius-wp-listing-category-custom-fields');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-varius-wp-activator.php
 */
function activate_varius_wp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-varius-wp-activator.php';
	Varius_Wp_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-varius-wp-deactivator.php
 */
function deactivate_varius_wp() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-varius-wp-deactivator.php';
	Varius_Wp_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_varius_wp' );
register_deactivation_hook( __FILE__, 'deactivate_varius_wp' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-varius-wp.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_varius_wp() {
    global $varius;

	$varius = new Varius_Wp();
	$varius->run();

}
run_varius_wp();

function varius_wp_plugin() {
    global $varius;
    return $varius;
}

require plugin_dir_path( __FILE__ ) . 'includes/class-varius-api.php';

function init_varius_api() {
    global $varius_api;

    // Wrap the VariusApi PHP library in a more WP-friendly API class
    $varius_api = new Varius_Wp_Api(new VariusApi(
        varius_wp_plugin()->get_setting('api-dealer-code'), 
        varius_wp_plugin()->get_setting('api-base-url'), 
        varius_wp_plugin()->get_setting('api-access-token')
    ));
}
init_varius_api();

function varius_api() {
    global $varius_api;
    return $varius_api;
}
