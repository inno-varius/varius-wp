<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.innovative.ink
 * @since      1.0.0
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/admin
 * @author     Innovative <dev@innovative.ink>
 */
class Varius_Wp_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * A place to store custom fields for the listing_category taxonomy.  Persistence is done via the WordPress Options API
	 */
	private $listing_category_custom_fields = array();

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->listing_category_custom_fields = get_option(VARIUS_WP_OPTION_LISTING_CATEGORY_CUSTOM_FIELDS);

	}

	private function update_listing_category_custom_fields() {
		update_option(VARIUS_WP_OPTION_LISTING_CATEGORY_CUSTOM_FIELDS, $this->listing_category_custom_fields);
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Varius_Wp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Varius_Wp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/varius-wp-admin.css', array(), $this->version, 'all' );

		wp_register_style( $this->plugin_name . '-select2', plugin_dir_url( __FILE__ ) . 'js/select2/select2.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Varius_Wp_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Varius_Wp_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/varius-wp-admin.js', array( 'jquery' ), $this->version, false );

		wp_register_script( $this->plugin_name . '-select2', plugin_dir_url( __FILE__ ) . 'js/select2/select2.js', array(), $this->version, false );

	}

/**************************************************************************************
 *
 *
 *
 *								LOCKDOWN LISITNGS
 *
 *
 *
 **************************************************************************************/

	public function remove_metaboxes_for_listings() {
		remove_meta_box( 'submitdiv', CPT_VARIUS_LISTING, 'side' );
		remove_meta_box( 'postcustom', CPT_VARIUS_LISTING, 'normal' );
	}

	public function disallow_new_listings() {
		global $_REQUEST, $pagenow;

		if ( ! empty($_REQUEST['post_type'])
				&& CPT_VARIUS_LISTING == $_REQUEST['post_type']
				&& ! empty($pagenow)
				&& 'post-new.php' == $pagenow )
		{
			wp_safe_redirect(admin_url('edit.php?post_type=' . CPT_VARIUS_LISTING));
		}
	}
		
	public function remove_add_new_menu_for_listings() {
		remove_submenu_page('edit.php?post_type=' . CPT_VARIUS_LISTING, 'post-new.php?post_type=' . CPT_VARIUS_LISTING);
	}
		
	public function change_add_new_button_for_listings() {
		global $post_new_file, $post_type_object;
  		
  		if ( ! isset($post_type_object) || CPT_VARIUS_LISTING != $post_type_object->name ) { 
  			return false; 
  		}

		$post_type_object->labels->add_new = 'Return to Index';
		$post_new_file = 'edit.php?post_type=' . CPT_VARIUS_LISTING;
	}
	
	public function prevent_posts_for_listings() {
		if ( ! empty($_POST) && $_POST['post_type'] == CPT_VARIUS_LISTING && $_POST['action'] == 'editpost' ) {
			if (true === DOING_AJAX) {
				exit;
			}
			
			if ( ! empty($_POST['post_ID']) ) {
				wp_safe_redirect(admin_url('post.php?post='.$_POST['post_ID'].'&action=edit'));
				exit;
			} else {
				wp_safe_redirect(admin_url('edit.php?post_type=' . CPT_VARIUS_LISTING));
				exit;
			}
		}
	}


	public function remove_listing_action_links($actions) {
		unset($actions['inline hide-if-no-js']);        // "quick edit"
        unset($actions['trash']);
        unset($actions['edit']);

        return $actions;
	}

	public function remove_edit_bulk_action_for_listings($actions) {
		unset($actions['edit']);

        return $actions;
	}

/**************************************************************************************
 *
 *
 *
 *								SETTTINGS
 *
 *
 *
 **************************************************************************************/

	public function on_update_settings($old, $new) {
        if ( $old['rewrite-path'] != $new['rewrite-path'] ) {
            set_transient(VARIUS_WP_DO_FLUSH_REWRITE_RULES, true);  // mark that a flush needs to occur
        }
    }

    public function flush_rewrite_rules() {
        flush_rewrite_rules();
    }

	public function add_settings_page() {
		add_options_page('Varius WP', 'Varius WP', 'manage_varius_settings', VARIUS_WP_SETTINGS, array($this, 'make_settings_page'));
	}	

	public function register_settings() {
		register_setting('varius-wp-settings-group', VARIUS_WP_SETTINGS);
		
		add_settings_section( 'general', 'General', '__return_empty_string', VARIUS_WP_SETTINGS );
		add_settings_field( 'rewrite-path', 'Rewrite Path', array($this, 'make_rewrite_path_field'), VARIUS_WP_SETTINGS, 'general' );

		add_settings_section( 'api', 'Varius API', '__return_empty_string', VARIUS_WP_SETTINGS );
		add_settings_field( 'api-auth-test', 'Connection Test', array($this, 'display_connection_test'), VARIUS_WP_SETTINGS, 'api' );
		add_settings_field( 'api-base-url', 'API Base URL', array($this, 'make_api_base_url_field'), VARIUS_WP_SETTINGS, 'api' );
		add_settings_field( 'api-access-token', 'API Access Token', array($this, 'make_api_access_token_field'), VARIUS_WP_SETTINGS, 'api' );
		add_settings_field( 'api-dealer-code', 'API Dealer Code', array($this, 'make_api_dealer_code_field'), VARIUS_WP_SETTINGS, 'api' );

		add_settings_field( 'api-last-sync', 'Last Synchronization', array($this, 'make_last_sync_field'), VARIUS_WP_SETTINGS, 'api' );

	}

	public function make_rewrite_path_field() { ?>
    	
    	<?php bloginfo('url') ?>/<input type='text' 
    									placeholder="some_url_path" 
    									name='<?php echo VARIUS_WP_SETTINGS; ?>[rewrite-path]' 
    									value='<?php echo varius_wp_plugin()->get_setting('rewrite-path'); ?>' />
    	<p class="description" id="rewrite-path-description">
    		The URL where you would like your Varius listings to be available at
    	</p>

    <?php 
	}

	public function display_connection_test($args) {
        $ok = true;
        $message = "";
        if ( ! varius_wp_plugin()->get_setting('api-base-url') ) {
            $ok = false;
            $message = "You need an API Base URL";
        } elseif ( ! varius_wp_plugin()->get_setting('api-access-token') ) {
            $ok = false;
            $message = "You need an API Access Token";
        } elseif ( ! varius_wp_plugin()->get_setting('api-dealer-code') ) {
            $ok = false;
            $message = "You need a Dealer Code";
        } else {
            try {
                $response = varius_api()->connection_test();
            } catch ( \GuzzleHttp\Exception\ConnectException $e ) {
                $ok = false;
                $message = "Could not connect to the API.  It is likely that your Base URI is incorrect, but it could also be that the Varius API server is currently inaccessible.";
            } catch ( \GuzzleHttp\Exception\RequestException $e ) {

                $ok = false;
                $message = "Unknown error";

                if ( $e->getResponse() ) {
                    $status = $e->getResponse()->getStatusCode();
                    $body = json_decode($e->getResponse()->getBody());

                    if ( $status == 404 ) {
                        if ( isset($body->error) && $body->error == 'unknown_dealer_code' ) {
                            $message = "Your Dealer Code was not recognized.";
                        } else {
                            $message = "Your Base URI seems to be incorrect.";
                        }
                    } elseif ( $status == 401 ) {
                        $message = "Your Access Token does not correspond to your Dealer Code.  One or the other is incorrect.";
                    }
                }

            }
        } ?><div id="varius-api-connection-test-result" class="<?php echo $ok ? 'success' : 'danger' ?>"><?php echo $ok ? 'Passed' : 'Failed<br>' ?></div>
        <p><?php echo $message ?></p>
        <p class="description" id="connection-test-description">Save Settings or Refresh this page to run the connection test again.</p><?php
    }

	public function make_api_base_url_field() { ?>

		<input type='url' 
				name='<?php echo VARIUS_WP_SETTINGS; ?>[api-base-url]' 
				value='<?php echo varius_wp_plugin()->get_setting('api-base-url'); ?>' />
		<p class="description" id="api-base-url-description">The Varius API base URL (trailing slash required) Eg. <code>https://portal.varius.ca/</code>.</p>

	<?php
	}

	public function make_api_access_token_field() { ?>

		<input type='text' 
				name='<?php echo VARIUS_WP_SETTINGS; ?>[api-access-token]' 
				value='<?php echo varius_wp_plugin()->get_setting('api-access-token'); ?>' 
				style="width: 100%"/>

	<?php
	}

	public function make_api_dealer_code_field() { ?>

		<input type='text' 
				name='<?php echo VARIUS_WP_SETTINGS; ?>[api-dealer-code]' 
				value='<?php echo varius_wp_plugin()->get_setting('api-dealer-code'); ?>' />

	<?php
	}

	public function make_last_sync_field() { 

		$last_sync = varius_api()->get_last_sync_time();
		if ( ! $last_sync ) {
			$last_sync = "You have not yet synchronized your Varius listings";
		}

		?>

		<p><code id="last-sync"><?php echo esc_html($last_sync); ?></code> <button id="button-sync-now" class="button button-secondary">Sync Now</button></p>

	<?php
	}

	public function make_settings_page() {
		include(plugin_dir_path( __FILE__ ) . 'partials/settings.php');
	}

/**************************************************************************************
 *
 *
 *
 *								TAXONOMY THINGS
 *
 *
 *
 **************************************************************************************/

	public function make_add_listing_category_custom_fields($term) { ?>

		<div class="form-field">
			<label for="associated_varius_types_<?php esc_attr_e($term->term_id) ?>">Associated Varius Types</label>
			<?php $this->make_associated_varius_types_field($term); ?>
		</div>

	<?php
	}

	public function make_edit_listing_category_custom_fields($term) { ?>

		<tr class="form-field">
			<th>
				<label for="associated_varius_types_<?php esc_attr_e($term->term_id) ?>">Associated Varius Types</label>
			</th>
			<td>
				<?php $this->make_associated_varius_types_field($term); ?>
			</td>
		</tr>

	<?php
	}

	public function make_associated_varius_types_field($term) { 
		wp_enqueue_script($this->plugin_name . '-select2');
		wp_enqueue_style($this->plugin_name . '-select2');
		?>
		<select class="select2" name="associated_varius_types[]" id="associated_varius_types_<?php esc_attr_e($term->term_id) ?>" multiple="multiple" style="width: 100%">
			<?php 
			$types = varius_api()->get_types();
			$term_custom_fields = null;
			if ( array_key_exists($term->term_id, $this->listing_category_custom_fields) ) {
				$term_custom_fields = $this->listing_category_custom_fields[$term->term_id];
			}
			foreach ( $types as $code => $name ) : 
				$selected = "";

				if ( $term_custom_fields ) {
					if ( in_array($code, $term_custom_fields['associated_varius_types']) ) {
						$selected = "selected";
					}
				}
				?>
		  		<option value="<?php esc_attr_e($code); ?>" <?php echo $selected; ?>><?php esc_html_e($name); ?></option>
		    <?php endforeach; ?>
		</select>
		<p class="description">Varius listings of the selected types above will be associated with this category</p>			
	<?php
	}

	public function save_listing_category_custom_fields($term_id) {
		if ( isset($_POST['associated_varius_types']) ) {

			if ( ! array_key_exists($term_id, $this->listing_category_custom_fields) ) {
				$this->listing_category_custom_fields[$term_id] = array();
			}

			$this->listing_category_custom_fields[$term_id]['associated_varius_types'] = $_POST['associated_varius_types'];

			$this->update_listing_category_custom_fields();
		}
	}

	public function delete_listing_category_custom_fields($term_id) {

		if ( array_key_exists($term_id, $this->listing_category_custom_fields) ) {

			unset($this->listing_category_custom_fields[$term_id]);

			$this->update_listing_category_custom_fields();
		}
	}

	public function add_varius_types_admin_menu() {
		add_submenu_page('edit.php?post_type=varius_listing', 'Varius Types', 'Varius Types', 'manage_varius_listing_categories', 'manage-varius-types', array( $this, 'make_varius_types_page' ));
	}

	public function make_varius_types_page() {
		$categories = get_terms(array('taxonomy' => TAX_LISTING_CATEGORY, 'hide_empty' => false));
		
		// Make a map of <varius type code> => array( <category_term_id>, ... ) so we can do quick lookups
		$map_type_to_categories = array();
		$category_custom_fields = $this->listing_category_custom_fields;
		foreach ( $category_custom_fields as $category_term_id => $custom_fields ) {

			if ( array_key_exists('associated_varius_types', $custom_fields) ) {
				$varius_type_codes = $custom_fields['associated_varius_types'];

				foreach ( $varius_type_codes as $code ) {
					if ( array_key_exists($code, $map_type_to_categories) ) {
						// If this is the first category_term_id we've encountered for this varius type code then start a new array
						$map_type_to_categories[$code] = array($category_term_id);
					} else {
						// Otherwise, just add to the array that's already there
						$map_type_to_categories[$code][] = $category_term_id;
					}
				}

			}

		}

		$varius_types = varius_api()->get_types();

		wp_enqueue_script($this->plugin_name . '-select2');
		wp_enqueue_style($this->plugin_name . '-select2');

		include(plugin_dir_path( __FILE__ ) . 'partials/page-manage-varius-types.php');
	}

/**************************************************************************************
 *
 *
 *
 *								AJAX THINGS
 *
 *
 *
 **************************************************************************************/

	public function ajax_script_sync_now() { 

		$screen = get_current_screen();
		if ( ! is_admin() || $screen->id != 'settings_page_' . VARIUS_WP_SETTINGS ) { return; }

		?>

		<script>
			jQuery(function() {
				var data = { 'action': 'sync-now' };

				jQuery('#button-sync-now').click(function(e) {
					var $_ = jQuery(this);
					$_.addClass("spin");
					jQuery.post(ajaxurl, data, function(response) {
						jQuery('#last-sync').text(response);
							$_.removeClass("spin");
					});
					e.stopPropagation();
					return false;
				});
			})
		</script>

	<?php
	}

	public function ajax_sync_now() {
		varius_api()->sync();
		echo varius_api()->get_last_sync_time();

		wp_die();
	}

}
