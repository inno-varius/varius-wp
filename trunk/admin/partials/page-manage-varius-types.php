<div class="wrap">
    <h1 class="wp-heading-inline">Varius Types</h1>
    <p>This is a list of "Listing Types" pulled from the Varius server.  You can associate these Types with your custom set of listing Categories.  Each Varius Listing has one Varius Type, and it will appear under any of your categories for which you have associated that Type.  You may associate one type with multiple Categories.</p>

    <table class="wp-list-table widefat fixed striped">
        <thead>
            <tr>
                <th scope="col">Type</th>
                <th scope="col">Associated Categories</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ( $varius_types as $varius_type_code => $varius_type_name ) : ?>
                <tr>
                    <td class="varius-type"><?php esc_html_e($varius_type_name) ?></td>
                    <td class="associated-categories">
                        <select class="select2" name="associated_categories[<?php esc_attr_e($varius_type_code); ?>][]" multiple="multiple" style="width: 100%">
                            <?php 
                            foreach ( $categories as $c ) : 
                                $selected = "";

                                if ( in_array($c->term_id, $map_type_to_categories[$varius_type_code]) ) {
                                    $selected = 'selected';
                                }
                                ?>
                                <option value="<?php esc_attr_e($c->term_id); ?>" <?php echo $selected; ?>><?php esc_html_e($c->name); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>