<div class="wrap">
    <h2>Varius WP Settings</h2>
    <form action="options.php" method="POST">
        <?php settings_fields( 'varius-wp-settings-group' ); ?>
        <?php do_settings_sections( 'varius-wp-settings' ); ?>
        <?php submit_button(); ?>
    </form>
</div>