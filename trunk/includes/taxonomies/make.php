<?php 

/**
 * All Custom Taxonomies should be defined in php files that simply return an array of the following format:
 *
 * [ 
 *     'name' => <taxonomy name>,
 *     'related_post_types' => <custom post types to which this taxonomy may be associated name>,
 *     'args' => <taxonomy args as specified by https://codex.wordpress.org/Function_Reference/register_taxonomy>,
 * ]
 */

return call_user_func(function () {
    
    $labels = array(
        'name'                       => _x( 'Makes', 'Taxonomy General Name', 'varius' ),
        'singular_name'              => _x( 'Make', 'Taxonomy Singular Name', 'varius' ),
        'menu_name'                  => __( 'Make', 'varius' ),
        'all_items'                  => __( 'All Makes', 'varius' ),
        'edit_item'                  => __( 'Edit Make', 'varius' ),
        'update_item'                => __( 'Update Make', 'varius' ),
        'view_item'                  => __( 'View Make', 'varius' ),
        'separate_items_with_commas' => __( 'Separate makes with commas', 'varius' ),
        'popular_items'              => __( 'Popular Makes', 'varius' ),
        'search_items'               => __( 'Search Makes', 'varius' ),
        'not_found'                  => __( 'Not Found', 'varius' ),
        'no_terms'                   => __( 'No Makes', 'varius' ),
        'items_list'                 => __( 'Makes list', 'varius' ),
        'items_list_navigation'      => __( 'Makes list navigation', 'varius' ),
    );
    $capabilities = array(
        'manage_terms'               => 'manage_varius_makes',
        'edit_terms'                 => 'edit_varius_makes',
        'delete_terms'               => 'delete_varius_makes',
        'assign_terms'               => 'assign_varius_makes',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    return array(
        'name' => TAX_MAKE,
        'related_post_types' => array( CPT_VARIUS_LISTING ),
        'args' => $args,
    );

});