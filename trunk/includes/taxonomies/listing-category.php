<?php 

/**
 * All Custom Taxonomies should be defined in php files that simply return an array of the following format:
 *
 * [ 
 *     'name' => <taxonomy name>,
 *     'related_post_types' => <custom post types to which this taxonomy may be associated name>,
 *     'args' => <taxonomy args as specified by https://codex.wordpress.org/Function_Reference/register_taxonomy>,
 * ]
 */

return call_user_func(function () {
    
    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'varius' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'varius' ),
        'menu_name'                  => __( 'Category', 'varius' ),
        'all_items'                  => __( 'All Categories', 'varius' ),
        'parent_item'                => __( 'Parent Category', 'varius' ),
        'parent_item_colon'          => __( 'Parent Category:', 'varius' ),
        'new_item_name'              => __( 'New Category Name', 'varius' ),
        'add_new_item'               => __( 'Add New Category', 'varius' ),
        'edit_item'                  => __( 'Edit Category', 'varius' ),
        'update_item'                => __( 'Update Category', 'varius' ),
        'view_item'                  => __( 'View Category', 'varius' ),
        'separate_items_with_commas' => __( 'Separate categories with commas', 'varius' ),
        'add_or_remove_items'        => __( 'Add or remove categories', 'varius' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'varius' ),
        'popular_items'              => __( 'Popular Categories', 'varius' ),
        'search_items'               => __( 'Search Categories', 'varius' ),
        'not_found'                  => __( 'Not Found', 'varius' ),
        'no_terms'                   => __( 'No Categories', 'varius' ),
        'items_list'                 => __( 'Categories list', 'varius' ),
        'items_list_navigation'      => __( 'Categories list navigation', 'varius' ),
    );
    $capabilities = array(
        'manage_terms'               => 'manage_varius_listing_categories',
        'edit_terms'                 => 'manage_varius_listing_categories',
        'delete_terms'               => 'manage_varius_listing_categories',
        'assign_terms'               => 'edit_varius_listings',
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'capabilities'               => $capabilities,
    );

    return array(
        'name' => TAX_LISTING_CATEGORY,
        'related_post_types' => array( CPT_VARIUS_LISTING ),
        'args' => $args,
    );

});