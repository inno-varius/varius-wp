<?php 

/**
 * All Custom Post Types should be defined in php files that simply return an array of the following format:
 *
 * [ 
 *     'name' => <custom post type name>,
 *     'args' => <post type args as specified by https://codex.wordpress.org/Function_Reference/register_post_type>,
 * ]
 */

return call_user_func(function() {

    $labels = array(
        'name'                  => _x( 'Listings', 'Post Type General Name', 'varius' ),
        'singular_name'         => _x( 'Listing', 'Post Type Singular Name', 'varius' ),
        'menu_name'             => __( 'Varius Listings', 'varius' ),
        'name_admin_bar'        => __( 'Varius Listing', 'varius' ),
        'archives'              => __( 'Listing Archives', 'varius' ),
        'attributes'            => __( 'Listing Attributes', 'varius' ),
        'parent_item_colon'     => __( 'Parent Listing:', 'varius' ),
        'all_items'             => __( 'All Listings', 'varius' ),
        'add_new_item'          => __( 'Add New Listing', 'varius' ),
        'add_new'               => __( 'Add New', 'varius' ),
        'new_item'              => __( 'New Listing', 'varius' ),
        'edit_item'             => __( 'Edit Listing', 'varius' ),
        'update_item'           => __( 'Update Listing', 'varius' ),
        'view_item'             => __( 'View Listing', 'varius' ),
        'view_items'            => __( 'View Listings', 'varius' ),
        'search_items'          => __( 'Search Listing', 'varius' ),
        'not_found'             => __( 'Not found', 'varius' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'varius' ),
        'featured_image'        => __( 'Featured Image', 'varius' ),
        'set_featured_image'    => __( 'Set featured image', 'varius' ),
        'remove_featured_image' => __( 'Remove featured image', 'varius' ),
        'use_featured_image'    => __( 'Use as featured image', 'varius' ),
        'insert_into_item'      => __( 'Insert into listing', 'varius' ),
        'uploaded_to_this_item' => __( 'Uploaded to this listing', 'varius' ),
        'items_list'            => __( 'Listings list', 'varius' ),
        'items_list_navigation' => __( 'Listings list navigation', 'varius' ),
        'filter_items_list'     => __( 'Filter listings list', 'varius' ),
    );
    $capabilities = array(
        'edit_post'             => 'edit_varius_listing',
        'read_post'             => 'read_post',
        'delete_post'           => 'delete_varius_listing',
        'edit_posts'            => 'edit_varius_listings',
        'edit_others_posts'     => 'edit_others_varius_listings',
        'publish_posts'         => 'publish_varius_listings',
        'read_private_posts'    => 'read_private_posts',
    );
    $rewrite = array(
        'slug'                  => varius_wp_plugin()->get_setting('rewrite-path'),
        'with_front'            => false,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __( 'Listing', 'varius' ),
        'description'           => __( 'A Varius listing', 'varius' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),
        'taxonomies'            => array( TAX_LISTING_CATEGORY ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => plugin_dir_url( dirname(dirname(__FILE__))  ) . 'admin/images/varius-dashicon.png',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capabilities'          => $capabilities,
        'rewrite'               => $rewrite,
    );

    return array(
        'name' => CPT_VARIUS_LISTING,
        'args' => $args
    );

});