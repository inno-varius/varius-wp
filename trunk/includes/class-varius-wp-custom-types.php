<?php

/**
 * The file responsible for defining cutsom post types and taxonomies
 *
 *
 * @link       https://www.innovative.ink
 * @since      1.0.0
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 */


define('CPT_VARIUS_LISTING', 'varius_listing');
define('TAX_LISTING_CATEGORY', 'listing_category');
define('TAX_MAKE', 'varius_make');

/**
 * The custom types class
 *
 * This is used to define custom post types and taxonomies
 *
 * @since      1.0.0
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 * @author     Innovative <dev@innovative.ink>
 */
class Varius_Wp_Custom_Types {

    public function register_custom_types() {

        foreach ( glob(plugin_dir_path( dirname( __FILE__ ) ) . 'includes/taxonomies/*.php') as $filename ) {
            $tax = require_once $filename;
            register_taxonomy($tax['name'], $tax['related_post_types'], $tax['args']);
        }

        foreach ( glob(plugin_dir_path( dirname( __FILE__ ) ) . 'includes/post_types/*.php') as $filename ) {
            $cpt = require_once $filename;
            register_post_type($cpt['name'], $cpt['args']);
        }
    }
}
