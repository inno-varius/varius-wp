<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.innovative.ink
 * @since      1.0.0
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 * @author     Innovative <dev@innovative.ink>
 */
class Varius_Wp_Activator {

    /**
    * Short Description. (use period)
    *
    * Long Description.
    *
    * @since    1.0.0
    */
    public static function activate() {

        $admin_role = get_role('administrator');
        $admin_role->add_cap('manage_varius_settings');
        $admin_role->add_cap('manage_varius_listing_categories');
        $admin_role->add_cap('manage_varius_makes');
        $admin_role->add_cap('edit_varius_listing');
        $admin_role->add_cap('edit_varius_listings');
        $admin_role->add_cap('delete_varius_listing');
        $admin_role->add_cap('edit_others_varius_listings');
        $admin_role->add_cap('publish_varius_listings');

        $editor_role = get_role('editor');
        $editor_role->add_cap('manage_varius_settings');
        $editor_role->add_cap('manage_varius_listing_categories');
        $editor_role->add_cap('manage_varius_makes');
        $editor_role->add_cap('edit_varius_listing');
        $editor_role->add_cap('edit_varius_listings');
        $editor_role->add_cap('delete_varius_listing');
        $editor_role->add_cap('edit_others_varius_listings');
        $editor_role->add_cap('publish_varius_listings');

    }

}
