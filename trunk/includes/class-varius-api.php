<?php

use VariusApi\VariusApi;

class Varius_Wp_Api {

    private $api;

    public function __construct(VariusApi $api) {
        $this->api = $api;
    }

    public function connection_test() {

        // make sure we can connect to the API
        $this->api->status();

        // also make sure we can authenticate
        $r = $this->api->authTest();

        return $r;
    }

    public function get_last_sync_time() {
        return get_option(VARIUS_WP_OPTION_LAST_SYNC);
    }

    public function get_types() {
        return unserialize(get_option(VARIUS_WP_OPTION_VARIUS_TYPES));
    }

    public function get_type_codes() {
        return array_keys($this->get_types());
    }

    public function sync() {
        $this->set_last_sync_to_now();

        $this->sync_types();
    }

    private function set_last_sync_to_now() {
        $d = date('Y-m-d H:i:s');
        update_option(VARIUS_WP_OPTION_LAST_SYNC, $d);
    }

    private function sync_types() {
        $r = $this->api->getTypes();

        $types = array();
        foreach ( $r->body->data as $el ) {
            $types[$el->code] = $el->name;
        }
        update_option(VARIUS_WP_OPTION_VARIUS_TYPES, serialize($types));
    }

   //  public function get_makes() {
   //      $ret = $this->api->getMakes();

   //      return $ret->body->data;
   //  }

   //  public function get_make_ranked($make_code, $min_count=1) {
   //      $ret = $this->api->getMakeRanked($make_code, $min_count);

   //      $make = $ret->body->data[0];
   //      if ( $make ) {
   //          $make->url = varius_wp_url(['make_code' => $make->code]);
   //      }
   //      return $make;
   //  }

   //  public function get_makes_ranked($min_count=1) {
   //      $ret = $this->api->getMakesRanked($min_count);

   //      $makes = $ret->body->data;

   //      foreach ($makes as $m) {
   //          $m->url = varius_wp_url(['make_code' => $m->code]);
   //      }

   //      return $makes;
   //  }

   //  public function get_types() {
   //      $ret = $this->api->getTypes();

   //      return $ret->body->data;
   //  }

   //  public function get_type_ranked($type_code, $min_count=1) {
   //      $ret = $this->api->getTypeRanked($type_code, $min_count);

   //      $T = $ret->body->data;
   //      return $ret->body->data;
   //  }

   //  public function get_typegroup_type_ranked($type_code, $typegroup_code="", $min_count=1) {
   //      $ret = $this->api->getApiTypegroupTypeRanked($type_code, $typegroup_code, $min_count);

   //      $T = $ret->body->data;
   //      return $ret->body->data;
   //  }

   //  public function get_typegroup_ranked($typegroup_code, $min_count=1) {
   //      $ret = $this->api->getApiTypegroupRanked($typegroup_code, $min_count);

   //      $G = $ret->body->data;

   //      $G->url = varius_wp_url(['typegroup_code' => $G->code]);

   //      foreach ( $G->types as $T ) {
   //          $T->url = varius_wp_url(['typegroup_code' => $G->code, 'type_code' => $T->code]);
   //      }

   //      return $G;
   //  }

   //  public function get_typegroups_ranked($min_count=1) {

   //      $ret = $this->api->getApiTypegroupsRanked($min_count);

   //      $typegroups = $ret->body->data->typegroups;

   //      foreach ( $typegroups as $G ) {
   //          $G->url = varius_wp_url(['typegroup_code' => $G->code]);

   //          foreach ( $G->types as $T ) {
   //              $T->url = varius_wp_url(['typegroup_code' => $G->code, 'type_code' => $T->code]);
   //          }
   //      }

   //      return $typegroups;
   //  }


   // public  function get_listing($stock_number) {
   //      $ret = $this->api->getListing($stock_number);

   //      $listing = $ret->body->data;
   //      $listing->url = varius_wp_url(['stock_number' => $listing->stock_number]);

   //      return $listing;
   //  }

   //  public function search_listings($search_params, &$pagination_data) {
   //      $search_params['typemodule_code'] = 'api';

   //      $ret = $this->api->searchListings($search_params);

   //      $listings = $ret->body->data;

   //      foreach ( $listings as $L ) {
   //          $L->url = varius_wp_url(['stock_number' => $L->stock_number]);
   //      }

   //      if ( isset($ret->body->pagination) ) {
   //          $pagination_data = $ret->body->pagination;
   //      } else {
   //          $pagination_data = null;
   //      }

   //      return $listings;
   //  }

}