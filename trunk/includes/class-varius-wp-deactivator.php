<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.innovative.ink
 * @since      1.0.0
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 * @author     Innovative <dev@innovative.ink>
 */
class Varius_Wp_Deactivator {

    /**
    * Short Description. (use period)
    *
    * Long Description.
    *
    * @since    1.0.0
    */
    public static function deactivate() {

        $admin_role = get_role('administrator');
        $admin_role->remove_cap('manage_varius_settings');
        $admin_role->remove_cap('manage_varius_listing_categories');
        $admin_role->remove_cap('manage_varius_makes');
        $admin_role->remove_cap('edit_varius_listing');
        $admin_role->remove_cap('edit_varius_listings');
        $admin_role->remove_cap('delete_varius_listing');
        $admin_role->remove_cap('edit_others_varius_listings');
        $admin_role->remove_cap('publish_varius_listings');

        $editor_role = get_role('editor');
        $editor_role->remove_cap('manage_varius_settings');
        $editor_role->remove_cap('manage_varius_listing_categories');
        $editor_role->remove_cap('manage_varius_makes');
        $editor_role->remove_cap('edit_varius_listing');
        $editor_role->remove_cap('edit_varius_listings');
        $editor_role->remove_cap('delete_varius_listing');
        $editor_role->remove_cap('edit_others_varius_listings');
        $editor_role->remove_cap('publish_varius_listings');
    }

}
