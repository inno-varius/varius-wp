<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://www.innovative.ink
 * @since      1.0.0
 *
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Varius_Wp
 * @subpackage Varius_Wp/includes
 * @author     Innovative <dev@innovative.ink>
 */
class Varius_Wp {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Varius_Wp_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	protected $settings = array();

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'varius-wp';

		$this->settings = get_option(VARIUS_WP_SETTINGS);

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->register_custom_types();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Varius_Wp_Loader. Orchestrates the hooks of the plugin.
	 * - Varius_Wp_i18n. Defines internationalization functionality.
	 * - Varius_Wp_Admin. Defines all hooks for the admin area.
	 * - Varius_Wp_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-varius-wp-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-varius-wp-i18n.php';

		/**
		 * The class responsible for defining custom post types and taxonomies
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-varius-wp-custom-types.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-varius-wp-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-varius-wp-public.php';

		$this->loader = new Varius_Wp_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Varius_Wp_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Varius_Wp_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Varius_Wp_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Lockdown listings
		$this->loader->add_action( 'do_meta_boxes', $plugin_admin, 'remove_metaboxes_for_listings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'disallow_new_listings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'remove_add_new_menu_for_listings' );
		$this->loader->add_action( 'admin_head', $plugin_admin, 'change_add_new_button_for_listings' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'prevent_posts_for_listings');
		$this->loader->add_filter( 'post_row_actions', $plugin_admin, 'remove_listing_action_links' );
		$this->loader->add_filter( 'bulk_actions-edit-' . CPT_VARIUS_LISTING, $plugin_admin, 'remove_edit_bulk_action_for_listings' );

		// Settings
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_settings_page' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_settings' );

		$this->loader->add_action( 'update_option_' . VARIUS_WP_SETTINGS, $plugin_admin, 'on_update_settings', 10, 2 );
		// We use a transient because flushing directly in on_update_settings doesn't actually wait.
        // We have to wait until the next page load for the flush to work.
        // See: http://wordpress.stackexchange.com/questions/210596/flush-rewrite-rules-not-working-when-settings-updated
		if ( delete_transient(VARIUS_WP_DO_FLUSH_REWRITE_RULES) ) {
            $this->loader->add_action('admin_init', $plugin_admin, 'flush_rewrite_rules');
        }

        // Taxonomy things
        $this->loader->add_action( TAX_LISTING_CATEGORY . '_edit_form_fields', $plugin_admin, 'make_edit_listing_category_custom_fields', 10, 2);
        $this->loader->add_action( TAX_LISTING_CATEGORY . '_add_form_fields', $plugin_admin, 'make_add_listing_category_custom_fields', 10, 2);
        $this->loader->add_action( 'edited_' . TAX_LISTING_CATEGORY, $plugin_admin, 'save_listing_category_custom_fields', 10, 2);
        $this->loader->add_action( 'created_' . TAX_LISTING_CATEGORY, $plugin_admin, 'save_listing_category_custom_fields', 10, 2);
        $this->loader->add_action( 'delete_' . TAX_LISTING_CATEGORY, $plugin_admin, 'delete_listing_category_custom_fields', 10, 2);

        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_varius_types_admin_menu' );


        // Ajax
        $this->loader->add_action( 'admin_footer', $plugin_admin, 'ajax_script_sync_now' );
        $this->loader->add_action('wp_ajax_sync-now', $plugin_admin, 'ajax_sync_now');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Varius_Wp_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	private function register_custom_types() {

		$this->loader->add_action( 'init', new Varius_Wp_Custom_Types(), 'register_custom_types' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Varius_Wp_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	public function get_setting($name, $value = null) {

        if( isset($this->settings[$name]) ) {
            $value = $this->settings[$name];
        }

        return $value;
    }

}
